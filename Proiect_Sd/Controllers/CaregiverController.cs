﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Services.Interfaces;
using Proiect_Sd.Utility;
using System.Threading.Tasks;

namespace Proiect_Sd.Controllers
{
    [ApiController]
    public class CaregiverController : ControllerBase
    {
        private readonly ICaregiverService _caregiverService;

        public CaregiverController(ICaregiverService caregiverService)
        {
            _caregiverService = caregiverService;
        }

        [ProducesResponseType(typeof(CaregiverResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Caregivers.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var caregivers = await _caregiverService.GetCaregivers();

            return Ok(caregivers);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(CaregiverResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Caregivers.Get, Name = ApiRoutes.Names.GetCaregiver)]
        public async Task<IActionResult> Get([FromRoute] int caregiverId)
        {
            var caregiver = await _caregiverService.GetCaregiverResponseByIdAsync(caregiverId);

            if(caregiver == null)
            {
                return NotFound();
            }

            return Ok(caregiver);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Caregivers.Create)]
        public async Task<IActionResult> Create([FromBody] CaregiverRequestDTO caregiverRequestDTO)
        {
            var caregiver = await _caregiverService.CreateCaregiverAsync(caregiverRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetCaregiver, new { caregiverId = caregiver.CaregiverId }, "New caregiver created");
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.Caregivers.Update)]
        public async Task<IActionResult> Update([FromRoute] int caregiverId, [FromBody] CaregiverRequestDTO caregiverRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _caregiverService.CheckIfCaregiverExistsAsync(caregiverId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _caregiverService.UpdateCaregiverAsync(caregiverRequestDTO, caregiverId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.Caregivers.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int caregiverId)
        {
            var exists = await _caregiverService.CheckIfCaregiverExistsAsync(caregiverId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _caregiverService.DeleteCaregiverAsync(caregiverId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
