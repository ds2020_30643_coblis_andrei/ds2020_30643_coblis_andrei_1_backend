﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Proiect_Sd.DTOs.Request;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Proiect_Sd.Utility;
using Proiect_Sd.Services;
using System.Threading.Tasks;
using System.Security.Claims;

namespace Proiect_Sd.Controllers
{
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IConfiguration _config;

        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IConfiguration config, IAuthenticationService authenticationService)
        {
            _config = config;
            _authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [HttpPost(ApiRoutes.Identity.Login)]
        public async Task<IActionResult> Login([FromBody] LoginRequestDTO loginRequestDTO)
        {
            IActionResult response = Unauthorized();
            var authenticated = await _authenticationService.CheckIfLoginCredentialsExist(loginRequestDTO);

            if (authenticated)
            {
                var tokenString = GenerateJSONWebToken(loginRequestDTO);
                response = Ok(new { token = tokenString });
            }

            return response;
        }

        private string GenerateJSONWebToken(LoginRequestDTO userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtOptions:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //var token = new JwtSecurityToken(_config["JwtOptions:Issuer"],
            //  _config["JwtOptions:Issuer"],
            //  Subject,
            //  expires: DateTime.Now.AddMinutes(120),
            //  signingCredentials: credentials);

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("Username", userInfo.Username),
                }),
                Expires = DateTime.Now.AddMinutes(120),
                Issuer = _config["JwtOptions:Issuer"],
                Audience = _config["JwtOptions:Issuer"],
                SigningCredentials = credentials
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
