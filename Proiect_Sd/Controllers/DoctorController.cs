﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Services.Interfaces;
using Proiect_Sd.Utility;
using System.Threading.Tasks;

namespace Proiect_Sd.Controllers
{
    [ApiController]
    public class DoctorController : ControllerBase
    {
        private readonly IDoctorService _doctorService;

        public DoctorController(IDoctorService doctorService)
        {
            _doctorService = doctorService;
        }

        [ProducesResponseType(typeof(DoctorResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Doctors.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var doctors = await _doctorService.GetDoctors();

            return Ok(doctors);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(DoctorResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Doctors.Get, Name = ApiRoutes.Names.GetDoctor)]
        public async Task<IActionResult> Get([FromRoute] int doctorId)
        {
            var doctor = await _doctorService.GetDoctorResponseByIdAsync(doctorId);

            if(doctor == null)
            {
                return NotFound();
            }

            return Ok(doctor);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Doctors.Create)]
        public async Task<IActionResult> Create([FromBody] DoctorRequestDTO doctorRequestDTO)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var doctor = await _doctorService.CreateDoctorAsync(doctorRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetDoctor, new { doctorId = doctor.DoctorId }, "New doctor created");
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.Doctors.Update)]
        public async Task<IActionResult> Update([FromRoute] int doctorId, [FromBody] DoctorRequestDTO doctorRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _doctorService.CheckIfDoctorExistsAsync(doctorId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _doctorService.UpdateDoctorAsync(doctorRequestDTO, doctorId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.Doctors.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int doctorId)
        {
            var exists = await _doctorService.CheckIfDoctorExistsAsync(doctorId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _doctorService.DeleteDoctorAsync(doctorId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
