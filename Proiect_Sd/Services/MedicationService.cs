﻿using AutoMapper;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using Proiect_Sd.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services
{
    public class MedicationService : IMedicationService
    {
        private readonly IMedicationRepository _medicationRepository;
        private readonly IMapper _mapper;

        public MedicationService(IMedicationRepository medicationRepository, IMapper mapper)
        {
            _medicationRepository = medicationRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfMedicationExistsAsync(int medicationId)
        {
            return await _medicationRepository.CheckIfMedicationExists(medicationId);
        }

        public async Task<Medication> CreateMedicationAsync(MedicationRequestDTO medicationRequestDTO)
        {
            var medication = _mapper.Map<Medication>(medicationRequestDTO);
            return await _medicationRepository.CreateMedication(medication);
        }

        public async Task<bool> DeleteMedicationAsync(int medicationId)
        {
            var medication = await GetMedicationByIdAsync(medicationId);

            return await _medicationRepository.DeleteMedication(medication);
        }

        public async Task<Medication> GetMedicationByIdAsync(int medicationId)
        {
            return await _medicationRepository.GetMedicationById(medicationId);
        }

        public async Task<MedicationResponseDTO> GetMedicationResponseByIdAsync(int medicationId)
        {
            return await _medicationRepository.GetMedicationResponseById(medicationId);
        }

        public async Task<List<MedicationResponseDTO>> GetMedications()
        {
            return await _medicationRepository.GetMedications();
        }

        public async Task<bool> UpdateMedicationAsync(MedicationRequestDTO medicationRequestDTO, int medicationId)
        {
            var medication = _mapper.Map<Medication>(medicationRequestDTO);
            medication.MedicationId = medicationId;

            return await _medicationRepository.UpdateMedication(medication);
        }
    }
}
