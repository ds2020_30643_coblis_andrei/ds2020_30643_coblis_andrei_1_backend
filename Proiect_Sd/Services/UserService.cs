﻿using AutoMapper;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfUserExistsAsync(int userId)
        {
            return await _userRepository.CheckIfUserExists(userId);
        }

        public async Task<User> CreateUserAsync(UserRequestDTO userRequestDTO)
        {
            var user = _mapper.Map<User>(userRequestDTO);
            return await _userRepository.CreateUser(user);
        }

        public async Task<bool> DeleteUserAsync(int userId)
        {
            var user = await GetUserByIdAsync(userId);

            return await _userRepository.DeleteUser(user);
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await _userRepository.GetUserById(userId);
        }

        public async Task<List<User>> GetUsers()
        {
            return await _userRepository.GetUsers();
        }

        public async Task<bool> UpdateUserAsync(UserRequestDTO userRequestDTO, int userId)
        {
            var user = _mapper.Map<User>(userRequestDTO);
            user.UserId = userId;

            return await _userRepository.UpdateUser(user);
        }
    }
}
