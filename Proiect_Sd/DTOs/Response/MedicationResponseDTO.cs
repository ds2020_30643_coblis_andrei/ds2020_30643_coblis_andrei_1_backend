﻿namespace Proiect_Sd.DTOs.Response
{
    public class MedicationResponseDTO
    {
        public int Id { get; set; }

        public int MedicationPlanId { get; set; }

        public string Name { get; set; }

        public string Dosage { get; set; }

        public string SideEffects { get; set; }
    }
}
