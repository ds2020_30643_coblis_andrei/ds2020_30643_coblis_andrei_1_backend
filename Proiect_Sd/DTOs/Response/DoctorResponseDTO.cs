﻿namespace Proiect_Sd.DTOs.Response
{
    public class DoctorResponseDTO
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }
    }
}
