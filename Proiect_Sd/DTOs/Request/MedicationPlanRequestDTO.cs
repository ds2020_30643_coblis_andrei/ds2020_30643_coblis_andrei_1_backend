﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.DTOs.Request
{
    public class MedicationPlanRequestDTO
    {
        [Required(ErrorMessage = "Please enter a patient ID")]
        public int PatientId { get; set; }

        [Required(ErrorMessage = "Please enter a start date")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Please enter an end date")]
        public DateTime EndDate { get; set; }
    }
}
