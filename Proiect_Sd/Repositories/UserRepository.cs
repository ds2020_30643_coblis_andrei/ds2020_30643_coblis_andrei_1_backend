﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Data;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;

        public UserRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfUserExists(int userId)
        {
            using(_context)
            {
                return await _context.Users.AnyAsync(u => u.UserId == userId);
            }
        }

        public async Task<User> CreateUser(User user)
        {
            using (_context)
            {
                _context.Users.Add(user);
                await _context.SaveChangesAsync();

                return user;
            }
        }

        public async Task<bool> DeleteUser(User user)
        {
            using(_context)
            {
                _context.Users.Remove(user);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<User> GetUserById(int userId)
        {
            using(_context)
            {
                return await _context.Users.FirstOrDefaultAsync(u => u.UserId == userId);
            }
        }

        public async Task<List<User>> GetUsers()
        {
            using(_context)
            {
                return await _context.Users.ToListAsync();
            }
        }

        public async Task<bool> UpdateUser(User user)
        {
            using(_context)
            {
                _context.Users.Update(user);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
