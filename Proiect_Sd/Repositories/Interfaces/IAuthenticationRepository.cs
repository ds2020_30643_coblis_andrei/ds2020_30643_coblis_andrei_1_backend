﻿using Proiect_Sd.DTOs.Request;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories.Interfaces
{
    public interface IAuthenticationRepository
    {
        Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO);
    }
}
