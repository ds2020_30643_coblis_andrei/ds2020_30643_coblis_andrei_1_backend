﻿using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories.Interfaces
{
    public interface IMedicationPlanRepository
    {
        Task<List<MedicationPlanResponseDTO>> GetMedicationPlans();
        Task<MedicationPlanResponseDTO> GetMedicationPlanResponseById(int medicationPlanId);
        Task<MedicationPlan> GetMedicationPlanById(int medicationPlanId);
        Task<MedicationPlan> CreateMedicationPlan(MedicationPlan medicationPlan);
        Task<bool> UpdateMedicationPlan(MedicationPlan medicationPlan);
        Task<bool> CheckIfMedicationPlanExists(int medicationPlanId);
        Task<bool> DeleteMedicationPlan(MedicationPlan medicationPlan);
    }
}
