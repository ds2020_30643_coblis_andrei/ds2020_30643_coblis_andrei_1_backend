﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Data;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Proiect_Sd.Repositories
{
    public class MedicationRepository : IMedicationRepository
    {
        private readonly DataContext _context;

        public MedicationRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfMedicationExists(int medicationId)
        {
            using(_context)
            {
                return await _context.Medications.AnyAsync(m => m.MedicationId == medicationId);
            }
        }

        public async Task<Medication> CreateMedication(Medication medication)
        {
            using(_context)
            {
                _context.Medications.Add(medication);
                await _context.SaveChangesAsync();

                return medication;
            }
        }

        public async Task<bool> DeleteMedication(Medication medication)
        {
            using(_context)
            {
                _context.Medications.Remove(medication);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<Medication> GetMedicationById(int medicationId)
        {
            using(_context)
            {
                return await _context.Medications.FirstOrDefaultAsync(m => m.MedicationId == medicationId);
            }
        }

        public async Task<MedicationResponseDTO> GetMedicationResponseById(int medicationId)
        {
            using (_context)
            {
                var medications = _context.Medications;

                var medicationResponseDTO = from medication in medications
                                            where medication.MedicationId == medicationId
                                            select new MedicationResponseDTO()
                                            {
                                                Id = medication.MedicationId,
                                                MedicationPlanId = medication.MedicationPlanId,
                                                Dosage = medication.Dosage,
                                                Name = medication.Name,
                                                SideEffects = medication.SideEffects
                                            };

                return await medicationResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<bool> UpdateMedication(Medication medication)
        {
            using(_context)
            {
                _context.Medications.Update(medication);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }

        public async Task<List<MedicationResponseDTO>> GetMedications()
        {
            using (_context)
            {
                var medications = _context.Medications;

                var medicationResponseDTOList = from medication in medications
                                            select new MedicationResponseDTO()
                                            {
                                                Id = medication.MedicationId,
                                                MedicationPlanId = medication.MedicationPlanId,
                                                Dosage = medication.Dosage,
                                                Name = medication.Name,
                                                SideEffects = medication.SideEffects
                                            };

                return await medicationResponseDTOList.ToListAsync();
            }
        }
    }
}
