﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.Entities
{
    public class Medication
    {
        [Key]
        public int MedicationId { get; set; }

        [Required]
        public int MedicationPlanId { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Required]
        [StringLength(15)]
        public string Dosage { get; set; }

        [Required]
        [StringLength(200)]
        public string SideEffects { get; set; }

        // Used for EntityFramework
        public MedicationPlan MedicationPlan { get; set; }
    }
}
