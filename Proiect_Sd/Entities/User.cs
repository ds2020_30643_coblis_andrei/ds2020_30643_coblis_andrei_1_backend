﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.Entities
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [StringLength(20)]
        [Required]
        public string Username { get; set; }

        [StringLength(20)]
        [Required]
        public string Password { get; set; }

        [StringLength(15)]
        [Required]
        public string Role { get; set; }
        
        [StringLength(20)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(20)]
        [Required]
        public string LastName { get; set; }

        [StringLength(50)]
        [Required]
        public string Address { get; set; }

        [StringLength(20)]
        [Required]
        public string Gender { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        // Used for EntityFramework
        public Patient Patient { get; set; }

        public Doctor Doctor { get; set; }

        public Caregiver Caregiver { get; set; }
    }
}
