﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.Entities
{
    public class Patient
    {
        [Key]
        public int PatientId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int CaregiverId { get; set; }

        [Required]
        [StringLength(100)]
        public string MedicalRecord { get; set; }

        // Used for EntityFramework
        public User User { get; set; }

        public Caregiver Caregiver { get; set; }

        public ICollection<MedicationPlan> MedicationPlans { get; set; }
    }
}
